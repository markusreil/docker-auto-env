package com.mreil.gradle.docker;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NonNull;

@Data
@Builder
@AllArgsConstructor
public class DockerEnv {
    @NonNull
    private String url;

    private boolean tls;

    private String certPath;
}
