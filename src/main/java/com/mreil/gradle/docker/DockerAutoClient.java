package com.mreil.gradle.docker;

import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.core.DockerClientBuilder;
import com.github.dockerjava.core.DockerClientConfig;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;

@Slf4j
public class DockerAutoClient {
    public DockerClient detect() throws IOException, InterruptedException {
        final DockerEnv dockerEnv =
                new DockerAutoEnv().detect();

        log.info("Detected docker env: " + dockerEnv);

        final String newUrl;
        if (isDockerJava2()) {
            newUrl = dockerEnv.getUrl()
                    .replaceFirst("^tcp",
                                  dockerEnv.isTls() ? "https" : "http");
        } else {
            newUrl = dockerEnv.getUrl()
                    .replaceFirst("^https?", "tcp");
        }

        final DockerClientConfig config =
                DockerClientConfig.createDefaultConfigBuilder()
                        .withUri(newUrl)
                        .withDockerCertPath(dockerEnv.getCertPath())
                        .build();
        return DockerClientBuilder.getInstance(config).build();
    }

    private boolean isDockerJava2() {
        try {
            Class.forName("com.github.dockerjava.api.DockerException");
            return true;
        } catch (ClassNotFoundException e){
            return false;
        }
    }

}
