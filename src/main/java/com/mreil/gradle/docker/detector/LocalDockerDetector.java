package com.mreil.gradle.docker.detector;

import com.mreil.gradle.docker.DockerEnv;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.IOException;

@Slf4j
public class LocalDockerDetector implements Detector {
    private final boolean isLocalDocker;

    public LocalDockerDetector() {
        isLocalDocker = detectLocalDocker();
    }

    private boolean detectLocalDocker() {
        Runtime r = Runtime.getRuntime();
        try {
            r.exec("docker");
            return true;
        } catch (IOException e) {
            log.debug(e.getMessage());
            return false;
        }
    }

    public boolean applies() {
        return isLocalDocker;
    }

    @Override
    public DockerEnv getEnv() {
        final String uri;
        if (new File("/var/run/docker.sock").exists()) {
            uri = "unix:///var/run/docker.sock";
        } else {
            uri = "http://localhost:2375";
        }

        log.info("Using local URL: " + uri);
        return DockerEnv.builder()
                        .url(uri)
                        .build();
    }

}
