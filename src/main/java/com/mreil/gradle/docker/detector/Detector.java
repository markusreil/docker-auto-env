package com.mreil.gradle.docker.detector;

import com.mreil.gradle.docker.DockerEnv;

public interface Detector {
    boolean applies();
    DockerEnv getEnv();
}
