package com.mreil.gradle.docker.detector;

import com.mreil.gradle.docker.DockerEnv;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Slf4j
public class DockerMachineDetector implements Detector {
    private final boolean isDockerMachine;

    public DockerMachineDetector() {
        isDockerMachine = detectDockerMachine();
    }

    private boolean detectDockerMachine() {
        Runtime r = Runtime.getRuntime();
        try {
            r.exec("docker-machine version");
            return true;
        } catch (IOException e) {
            log.debug(e.getMessage());
            return false;
        }
    }

    public boolean applies() {
        return isDockerMachine;
    }

    @Override
    public DockerEnv getEnv() {
        final Map<String, String> env = getDockerMachineEnv();

        String protocol = "http";
        if (env.get("DOCKER_TLS_VERIFY").equals("1")) {
            protocol = "https";
        }
        String url = protocol + env.get("DOCKER_HOST").replace("tcp", "");

        return DockerEnv.builder()
                        .url(url)
                        .certPath(env.get("DOCKER_CERT_PATH"))
                .build();
    }

    private Map<String, String> getDockerMachineEnv() {
        try {
            Runtime r = Runtime.getRuntime();
            Process p = r.exec("docker-machine env --shell bash");
            p.waitFor();
            List<String> lines = IOUtils.readLines(p.getInputStream());
            Map<String, String> env = new HashMap<>();
            Pattern pt = Pattern.compile("export ([^=]+)=\"?([^\"]*)\"?");

            for (String line : lines) {
                Matcher m = pt.matcher(line);
                if (m.matches()) {
                    env.put(m.group(1), m.group(2));
                }
            }
            return env;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
