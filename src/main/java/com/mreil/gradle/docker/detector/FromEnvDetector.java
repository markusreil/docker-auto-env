package com.mreil.gradle.docker.detector;

import com.mreil.gradle.docker.DockerEnv;

import java.util.HashMap;
import java.util.Map;

// TODO write int test
public class FromEnvDetector implements Detector {
    private final Map<Variables, String> dockerEnv = new HashMap<>();

    public FromEnvDetector() {
        final Map<String, String> env = System.getenv();

        for (Variables variable : Variables.values()) {
            if (env.containsKey(variable.name())) {
                dockerEnv.put(variable, env.get(variable.name()));
            }
        }
    }

    public boolean applies() {
        return !dockerEnv.isEmpty();
    }

    @Override
    public DockerEnv getEnv() {
        final String url = dockerEnv.get(Variables.DOCKER_HOST);
        final boolean tls = "1".equals(dockerEnv.get(Variables.DOCKER_TLS_VERIFY));

        return DockerEnv.builder()
                        .url(url)
                        .tls(tls)
                        .certPath(dockerEnv.get(Variables.DOCKER_CERT_PATH))
                        .build();
    }

    private enum Variables {
        DOCKER_CERT_PATH,
        DOCKER_HOST,
        DOCKER_MACHINE_NAME,
        DOCKER_TLS_VERIFY
    }
}
