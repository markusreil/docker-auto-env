package com.mreil.gradle.docker;

import com.google.common.collect.ImmutableList;
import com.mreil.gradle.docker.detector.Detector;
import com.mreil.gradle.docker.detector.DockerMachineDetector;
import com.mreil.gradle.docker.detector.FromEnvDetector;
import com.mreil.gradle.docker.detector.LocalDockerDetector;

import java.io.IOException;
import java.util.List;

public class DockerAutoEnv {
    public DockerEnv detect() throws IOException, InterruptedException {
        List<Detector> detectors = ImmutableList.of(
                new FromEnvDetector(),
                new DockerMachineDetector(),
                new LocalDockerDetector()
        );

        final DockerEnv dockerEnv =
                detectors.stream()
                         .filter(Detector::applies)
                         .findFirst()
                         .orElseThrow(() -> new RuntimeException("Could not find docker config"))
                         .getEnv();

       return dockerEnv;
    }

}
