package com.mreil.gradle.docker;

import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertNotNull;

public class DockerAutoClientTest {
    @Test
    public void testEnv() throws IOException, InterruptedException {
        final DockerAutoEnv auto = new DockerAutoEnv();
        final DockerEnv client = auto.detect();

        System.out.println(client);

        assertNotNull(client);
    }
}
