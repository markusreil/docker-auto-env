package com.mreil.gradle.docker;

import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.command.CreateContainerResponse;
import com.github.dockerjava.core.command.PullImageResultCallback;
import org.junit.Test;

import java.io.IOException;

public class DockerAutoEnvTest {
    @Test
    public void testEnv() throws IOException, InterruptedException {
        final DockerAutoClient auto = new DockerAutoClient();
        final DockerClient client = auto.detect();

        System.out.println(client);

        client.pullImageCmd("hello-world")
              .exec(new PullImageResultCallback()).awaitCompletion();

        final CreateContainerResponse hello = client.createContainerCmd("hello-world")
                                                 .exec();

        client.startContainerCmd(hello.getId()).exec();
        client.waitContainerCmd(hello.getId()).exec();
    }
}
